package com.inno.studinsky.semaphore;

public class Kvadrator extends Thread{
    int yArg;
    private String name; // название потока
    Semaphore semaphore; // объект для синхронизации

    public Kvadrator(int numberThred, int yArg, Semaphore semaphore) {
        this.yArg = yArg;
        this.name = "Kvadrator " + numberThred;
        this.semaphore = semaphore;
        start();
    }
    @Override
    public void run() {
        try {
            Thread.sleep(1000);
            int currentResult = yArg*yArg;
            semaphore.save(currentResult);
            System.out.println("поток " + this.name + " : обсчет значения Y = " + yArg + " результат расчет Y2 = " + currentResult);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
