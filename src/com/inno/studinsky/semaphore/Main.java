package com.inno.studinsky.semaphore;

public class Main {
    public static void main(String[] args) {
        // тестовые массивы числе для обсчета выражений x3 + y2 + z
        int[] masX = {1, 3, 6, 2, 7};
        int[] masY = {3, 8, 2, 5, 4};
        int[] masZ = {7, 4, 6, 2, 1};
        // создание объекта симафора и запуск потоков расчета
        Semaphore semaphore = new Semaphore(masX, masY, masZ);
        int i = 0;
        for (i = 0; i < masX.length; i++)
            new Cubator(i + 1, masX[i], semaphore);
        for (i = 0; i < masY.length; i++)
            new Kvadrator(i + 1, masY[i], semaphore);
        for (i = 0; i < masZ.length; i++)
            new Prostator(i + 1, masZ[i], semaphore);

        // ожидание окончания всех потоков расчета для вывода итоговых сумм
        while (semaphore.getNeedCalculate() > 0) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("итоговая сумма X3 = " + semaphore.getResultX());
        System.out.println("итоговая сумма Y2 = " + semaphore.getResultY());
        System.out.println("итоговая сумма Z = " + semaphore.getResultZ());
        int resultCalc = semaphore.getResultX() + semaphore.getResultY() + semaphore.getResultZ();
        System.out.println("Общая сумма результатов всех выражений x3 + y2 + z = " + resultCalc);
    }
}
