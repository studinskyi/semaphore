package com.inno.studinsky.semaphore;

public class Semaphore {
    private int[] masX;
    private int[] masY;
    private int[] masZ;
    private int resultX = 0;
    private int resultY = 0;
    private int resultZ = 0;
    int needCalculate;

    public Semaphore(int[] masX, int[] masY, int[] masZ) {
        this.masX = masX;
        this.masY = masY;
        this.masZ = masZ;
        this.needCalculate = masX.length + masY.length + masZ.length;
    }

    public void save(int resCulc) {
        Thread currentThread = Thread.currentThread();
        Class objectClass = Thread.currentThread().getClass();
        // синхронизированный в рамках одного класса по объекту типа Class
        synchronized (objectClass) {
            if (currentThread instanceof Cubator) {
                resultX = resultX + resCulc;
                System.out.println("Semaphore текущий X = " + resCulc + " общая сумма X3 = " + resultX);
            }
            if (currentThread instanceof Kvadrator) {
                resultY = resultY + resCulc;
                System.out.println("Semaphore текущий Y = " + resCulc + " общая сумма Y2 = " + resultY);
            }
            if (currentThread instanceof Prostator) {
                resultZ = resultZ + resCulc;
                System.out.println("Semaphore текущий Z = " + resCulc + " общая сумма Z = " + resultZ);
            }
        }
        // уменьшение счетчика оставшихся для расчета данных
        needCalculate = needCalculate - 1;
    }

    public int getResultX() {
        return resultX;
    }

    public int getResultY() {
        return resultY;
    }

    public int getResultZ() {
        return resultZ;
    }

    public int getNeedCalculate() {
        return needCalculate;
    }
}
