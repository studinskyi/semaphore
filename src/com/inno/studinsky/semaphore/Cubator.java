package com.inno.studinsky.semaphore;

public class Cubator extends Thread {
    int xArg;
    private String name; // название потока
    Semaphore semaphore; // объект для синхронизации

    public Cubator(int numberThred, int xArg, Semaphore semaphore) {
        this.xArg = xArg;
        this.name = "Cubator " + numberThred;
        this.semaphore = semaphore;
        start();
    }
    @Override
    public void run() {
        try {
            Thread.sleep(1000);
            int currentResult = xArg*xArg*xArg;
            semaphore.save(currentResult);
            System.out.println("поток " + this.name + " : обсчет значения X = " + xArg + " результат расчет X3 = " + currentResult);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
