package com.inno.studinsky.semaphore;

public class Prostator extends Thread{
    int zArg;
    private String name; // название потока
    Semaphore semaphore; // объект для синхронизации

    public Prostator(int numberThred, int zArg, Semaphore semaphore) {
        this.zArg = zArg;
        this.name = "Prostator " + numberThred;
        this.semaphore = semaphore;
        start();
    }
    @Override
    public void run() {
        try {
            Thread.sleep(1000);
            semaphore.save(zArg);
            System.out.println("поток " + this.name + " : обсчет значения Z = " + zArg + " результат расчет Z = " + zArg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
